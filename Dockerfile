FROM nginx:alpine
#RUN sed -i 's/server {/server {\n  server_tokens off; /g' \
#/etc/nginx/conf.d/default.conf 
EXPOSE 80
EXPOSE 443
CMD ["nginx", "-g", "daemon off;"]

